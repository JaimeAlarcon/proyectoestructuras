/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.main;

import espol.edu.ec.tools.StackFn;
import espol.edu.ec.tdas.Instruccion;
import espol.edu.ec.tdas.Operando;
import espol.edu.ec.tools.Reader;
import espol.edu.ec.tools.Writer;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Stack;

/**
 *
 * @author elabandonao
 */
public class Test {
    public static void main(String[] args) {
        ArrayList<Operando> operandos = new ArrayList<>();
        for (ArrayList<String> l : Reader.getOperandos("memoria.txt")) {
            Operando o = new Operando(l.get(0), Integer.parseInt(l.get(1)));
            operandos.add(o);            
        }
        
        ArrayList<Instruccion> instrucciones = new ArrayList<>();
        boolean existe;
        Instruccion i;
        for (ArrayList<String> l : Reader.getInstrucciones("instrucciones.txt")) {
            existe = false;
            i = null;
            for (Operando o : operandos) {
                if (l.get(1).equals(o.getDireccion())) {
                    existe = true;
                    i = new Instruccion(l.get(0), o, Integer.parseInt(l.get(2)));
                }
            }
            
            if (!existe) {
                if (l.get(1) == " ") {
                    i = new Instruccion(l.get(0), new Operando(), Integer.parseInt(l.get(2)));
                }
                else {
                    Operando o = new Operando(l.get(1), 0);
                    operandos.add(o);
                    i = new Instruccion(l.get(0), o, Integer.parseInt(l.get(2)));
                }
            }
            
            instrucciones.add(i);
        }     
        
        Stack<Integer> instruccionesStack = new Stack<>();
        PriorityQueue<Instruccion> instruccionesPriorityQueue = new PriorityQueue<>((i1, i2) -> (i1.getPrioridad() - i2.getPrioridad()));
        instruccionesPriorityQueue.addAll(instrucciones);
        
        while (!instruccionesPriorityQueue.isEmpty()) {
            StackFn.realizarOperacion(instruccionesStack, instruccionesPriorityQueue.poll());

        }
        
        Writer.write(operandos);
    }
}
