/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.tdas;

/**
 *
 * @author elabandonao
 */
public class Instruccion {
    private String operador;
    private Operando operando;
    private int prioridad;
    
    public Instruccion(String operador, Operando operando, int prioridad) {
        this.operador = operador;
        this.operando = operando;
        this.prioridad = prioridad;
    }

    public String getOperador() {
        return operador;
    }

    public Operando getOperando() {
        return operando;
    }

    public int getPrioridad() {
        return prioridad;
    }

        
    @Override
    public String toString() {
        return "Instruccion{" + operador + ", operando=" + operando + ", prioridad=" + prioridad + "\n}";
    }

    
    
    
}
