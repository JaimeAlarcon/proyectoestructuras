package espol.edu.ec.tdas;

/**
 *
 * @author elabandonao
 */
public class Operando
{
    private String direccion;
    private int valor;

    public Operando() {
        direccion = "";
        valor = 0;
    }
    
    public Operando(String direccion, int valor){
        this.direccion = direccion;
        this.valor = valor;
    }

    public String getDireccion() {
        return direccion;
    }

    public int getValor() {
        return valor;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }
    
    
    
    @Override
    public String toString() {
        return "Operando{" + "direccion=" + direccion + ", valor=" + valor + "}\n";
    }
    
    public String toStringEscritura()
    {
        return direccion + "|" + valor;
    }
}
