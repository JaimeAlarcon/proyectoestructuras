package espol.edu.ec.tools;

import espol.edu.ec.tdas.Instruccion;
import java.util.Stack;

public class StackFn 
{
    
    //HOLA ESTO ES UNA PRUEBA
    public static void realizarOperacion(Stack<Integer> pila, Instruccion valor) {
        switch(valor.getOperador())
        {
            case ("PUSH"):
                pila.push(valor.getOperando().getValor());
                break;
            case ("POP"):
                if (!pila.isEmpty()) {
                    valor.getOperando().setValor(pila.pop());
                }
                break;
            case ("ADD"):
                if (pila.size() >= 2) {
                    int n1 = pila.pop();
                    int n2 = pila.pop();
                    int suma = n1 + n2;
                    pila.push(suma);
                break;
            }
            case ("DIF"):
                if (pila.size() >= 2) {
                    int n1 = pila.pop();
                    int n2 = pila.pop();
                    int dif = n1 - n2;
                    pila.push(dif);
                }
                break;
            case ("MUL"):
                if (pila.size() >= 2) {
                    int n1 = pila.pop();
                    int n2 = pila.pop();
                    int mul = n1 * n2;
                    pila.push(mul);
                }
                break;
            case ("DIV"):
                if (pila.size() >= 2) {
                    int n1 = pila.pop();
                    int n2 = pila.pop();
                    int div = n1 / n2;
                    pila.push(div);
                }
                break;
        }
        
    }
}
