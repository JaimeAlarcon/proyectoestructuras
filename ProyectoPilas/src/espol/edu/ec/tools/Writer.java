package espol.edu.ec.tools;

import espol.edu.ec.tdas.Operando;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author elabandonao
 */
public class Writer 
{
    public static void write(ArrayList<Operando> operandos) {
        for (Operando o : operandos) {
            if (o.getDireccion().equals("")) {
                operandos.remove(o);
            }
        }
        try(BufferedWriter bw = new BufferedWriter(new FileWriter("memoria.txt"))) {
            for (Operando o : operandos) {
                bw.write(o.toStringEscritura());
                bw.newLine();
                bw.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
