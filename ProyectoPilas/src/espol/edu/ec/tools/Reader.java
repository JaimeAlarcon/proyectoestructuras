package espol.edu.ec.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author elabandonao
 */
public class Reader {
    public static ArrayList<ArrayList<String>> getOperandos(String url) {
        File file = new File(url);
        ArrayList<ArrayList<String>> text = new ArrayList<>();
        try{
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                ArrayList<String> temp = new ArrayList<>();
                String line = sc.nextLine();
                temp.add(line.substring(0, line.indexOf("|")));
                temp.add(line.substring(line.indexOf("|") + 1));
                
                text.add(temp);
            }
            
                return text;
                
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public static ArrayList<ArrayList<String>> getInstrucciones(String url) {
        File file = new File(url);
        ArrayList<ArrayList<String>> text = new ArrayList<>();
        try{
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                ArrayList<String> temp = new ArrayList<>();
                String line = sc.nextLine();
                temp.add(line.substring(0, line.indexOf("|")));
                String temp2 = (line.substring(line.indexOf("|") + 1));
                temp.add(temp2.substring(0, temp2.indexOf("|")));
                temp.add(temp2.substring(temp2.indexOf("|") + 1));
                text.add(temp);
            }
            
                return text;
                
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
